import React from 'react';
import '../css/style.css'

const Square = (props)=> {
    
    return <button className="square" onClick={props.onClick} value={props.value}>
        {props.content}
    </button>
    
}

export default Square;