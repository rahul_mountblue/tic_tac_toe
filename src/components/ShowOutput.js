import { React } from "react";
import '../css/style.css'

const ShowOutput = (props)=>{
    console.log(props);

    return <div className="show">
        <span className="close" onClick={props.onClick}>&#x274C;</span>
       
        <h3>Congratualtion : {props.winner}</h3>
        <p className="winner-text">
            You won
        </p>
    </div>
}

export default ShowOutput;