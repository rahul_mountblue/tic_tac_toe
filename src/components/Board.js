import { React, useState } from 'react';
import Square from './Square';
import '../css/style.css';
import ShowOutput from './ShowOutput';


const Board = () => {
    const [value, setValue] = useState([null, null, null,
        null, null, null,
        null, null, null])

    const [currPlayer, setPlayer] = useState('X');
    const [winner, setWinner] = useState(null);
    
    // calculate winner  function

    function calculateWinner(squares) {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];

        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                return squares[a];
            }
        }
        return null;
    }
    // handle click function to handle onclick event on button
    function handleClick(event) {

        let newArr = [...value];
        console.log(event.target.text);
        newArr[event.target.value] = currPlayer;
        setValue(newArr);

        setWinner(calculateWinner(newArr));
        currPlayer === 'X' ? setPlayer('0') : setPlayer('X');
    }

    // clear all the value

    const clearAll = () => {
        let arr = [null, null, null,
            null, null, null,
            null, null, null]
        setValue(arr);
        setPlayer('X');
        setWinner(null);
    }
   
    // close button after win

    const closeModel = () => {
        setWinner(null);
    }


    return <div className="container">
        <div className="header">Player Turn : {currPlayer}</div>
        <div className="board-row">
            <Square content={value[0]} value='0' onClick={handleClick} />
            <Square content={value[1]} value='1' onClick={handleClick} />
            <Square content={value[2]} value='2' onClick={handleClick} />
        </div>
        <div className="board-row">
            <Square content={value[3]} value='3' onClick={handleClick} />
            <Square content={value[4]} value='4' onClick={handleClick} />
            <Square content={value[5]} value='5' onClick={handleClick} />
        </div>
        <div className="board-row">
            <Square content={value[6]} value='6' onClick={handleClick} />
            <Square content={value[7]} value='7' onClick={handleClick} />
            <Square content={value[8]} value='8' onClick={handleClick} />
        </div>
        <button onClick={clearAll} className="clear-btn">restart</button>

        {winner !== null ? <ShowOutput onClick={closeModel} winner={winner} /> : null}
    </div>
}


export default Board;